/*
 *Silverline Design Inc
 *Xeebus Demo Code
 *Send GPS coordinates to xeebus server continually 
 *LED 13 and LED12 indicate the status of Successfull post and GPRS setting re-initiate process
*/

#include <stdio.h>
#include <SoftwareSerial.h>
#define DATABUFFERSIZE 100

/* 
LED attached to pin 13 indicates response of server 
if LED = ON => Data successfully posted
if LED = OFF => Data not successfully posted
*/
const int ledPin = 13; 

/*
LED attached to pin 12 indicate that GPRS re-initate function is being executed.
*/
const int ledPin1 = 12;

char dataBuffer[DATABUFFERSIZE+1]; 
char GprsdataBuffer[DATABUFFERSIZE+1]; 
char CGGATdataBuffer[DATABUFFERSIZE+1];
char startChar = '$';   // character to define the start of NMEA string 
char startChar1 = '+';  // character to define the start of AT command respose from the GPSRS module 
char endChar = '\n';    // character to define the end of NMEA string
char endChar1 = '2';    // character to define the end of AT command response
boolean storeString = false;
SoftwareSerial GpsSerial(10, 11); 
SoftwareSerial GprsSerial(7, 8);
char *a, *b, *c, *d, *e,*f,*g,*h,*i,*j;

void setup()
{ 
   digitalWrite(ledPin,LOW);
   pinMode(ledPin1,OUTPUT);
   digitalWrite(ledPin1,HIGH);
   Serial.begin(9600);
   GprsSerial.begin(9600);

   Serial.println("Configguring SIM 900");
   GprsSerial.println("AT+CGATT?"); 
   delay(2000);  // attach or detach from GPRS service //if = 1, that means GPRS service is active over SIm and 0 means not.
   if(GprsSerial.available()){
     CGGAT();
     Serial.write(CGGATdataBuffer);
    }
   
    
   GprsSerial.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\""); 
   delay(2000); // bearer settings
  // toSerial();


 
    GprsSerial.println("AT+SAPBR=3,1,\"APN\",\"rcomnet\""); //here rcomnet is the APN name for reliance in Delhi, you have to change it to the APNname for your provider
    delay(2000);
    //toSerial();

 
    GprsSerial.println("AT+SAPBR=1,1");
    delay(2000);
   // toSerial();
    GpsSerial.begin(9600);


}

void loop(){
  if(getSoftwareSerialstring()){
    //GprsSerial.println(dataBuffer);
    if(strstr(dataBuffer,"$GPGGA")){
      Serial.println(dataBuffer);
      char FinalDatatobeSend[500];
      char *a, *b, *c, *d, *e,*f,*g,*h,*i,*j;
      a = strtok(dataBuffer,",");
      b = strtok(NULL,",");
      c = strtok(NULL,",");
      d = strtok(NULL,",");
      e = strtok(NULL,",");
      f = strtok(NULL,",");
      g = strtok(NULL,",");
      h = strtok(NULL,",");
      i = strtok(NULL,",");
      j = strtok(NULL,",");

     
     GprsSerial.listen();
        
      char *URLdatatobeSend="AT+HTTPPARA=\"URL\",\"http://xeebus.silverline-da.com/slvr_update_xeebus_location.php?id=1&latitude=%s&latDir=%s&longitude=%s&lonDir=%s&&altitude=%s\"";
      // char *URLdatatobeSend="AT+HTTPPARA=\"URL\",\"http://requestb.in/mzqsddmz?id=1&latitude=%s&latDir=%s&longitude=%s&lonDir=%s&&altitude=%s\"";
      sprintf(FinalDatatobeSend,URLdatatobeSend,c,d,e,f,i);
      
      // initialize http service
      GprsSerial.println("AT+HTTPINIT");
      delay(1000); 
      toSerial();

      GprsSerial.println("AT+HTTPPARA=\"CID\",1");
      delay(500);
    


      GprsSerial.println(FinalDatatobeSend); //string embedded with sensor value
      delay(1000);
      toSerial();

      // set http action type 0 = GET, 1 = POST, 2 = HEAD
      GprsSerial.println("AT+HTTPACTION=1");
      delay(2000);
      
     /* if(GprsSerial.available()){ 
      if(getSoftwareSerialGPRSstring()){
      Serial.write("\n ################################## \n");
      Serial.write(GprsdataBuffer);
      Serial.write("\n ################################## \n");
      }
      }
     */
    // toSerial();

      // read server response
      GprsSerial.println("AT+HTTPREAD");
      delay(4000);
      if(GprsSerial.available()){ 
      getSoftwareSerialGPRSstring();
      Serial.write("\n ################################## \n");
      Serial.write(GprsdataBuffer);
      Serial.write("\n ################################## \n");
       
      if(strstr(GprsdataBuffer,"200") || strstr(GprsdataBuffer,"30")){
      digitalWrite(ledPin, HIGH);
      Serial.write("Received Succesfull POST reply from Server");
      Serial.write("\n ################################## \n");
      
       }
       else {
         digitalWrite(ledPin,LOW);
         digitalWrite(ledPin1,LOW);
         reinitiate();
         digitalWrite(ledPin1,HIGH);
              }
       
      }
      
      
      
      //toSerial();
   
      GprsSerial.println("\n");
      GprsSerial.println("AT+HTTPTERM");
      toSerial();
      delay(300);

      GprsSerial.println("");
      //delay(1000);
      toSerial();
      GpsSerial.listen();
   
    }
  }
}
void toSerial()
{
  while(GprsSerial.available()!=0)
  {
    Serial.write(GprsSerial.read());
  }
}


/* Function to get the incoming GPS string */

boolean getSoftwareSerialstring(){
static byte dataBufferIndex = 0;

  while(GpsSerial.available()>0){
    char incomingByte = GpsSerial.read();
    if(incomingByte ==startChar){

      dataBufferIndex = 0;
      storeString =true;
    }
    if(storeString){
      if(dataBufferIndex == DATABUFFERSIZE){
        dataBufferIndex = 0;
        break;
      }
      if(incomingByte==endChar){
        dataBuffer[dataBufferIndex] = 0;
        return true;
      }
      else{
        dataBuffer[dataBufferIndex++]=incomingByte;
        dataBuffer[dataBufferIndex] = 0;
      }
    }
    else{
    }
  }
  return false;
} 

/* Function to get the output of HTTPACTION , in order to know physically through LED , whether Data has been succesfully POSTED on server or not */

boolean getSoftwareSerialGPRSstring(){
static byte dataBufferIndex = 0;

  while(GprsSerial.available()>0){
    char incomingByte = GprsSerial.read();
    if(incomingByte ==startChar1){

      dataBufferIndex = 0;
      storeString =true;
    }
    if(storeString){
      if(dataBufferIndex == DATABUFFERSIZE){
        dataBufferIndex = 0;
        break;
      }
      if(incomingByte==endChar1){
        GprsdataBuffer[dataBufferIndex] = 0;
        return true;
      }
      else{
        GprsdataBuffer[dataBufferIndex++]=incomingByte;
        GprsdataBuffer[dataBufferIndex] = 0;
      }
    }
    else{
    }
  }
  return false;
}
/* Function to reinitiate the settings of GPRS in order to start HTTP  */

void reinitiate(){
   
   Serial.write("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n");
   Serial.println("Configguring SIM 900 again");
   GprsSerial.println("AT+CGATT?"); 
   delay(2000);  
   toSerial();
   
 
  
   GprsSerial.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\""); 
   delay(2000); // bearer settings
   toSerial();


 
    GprsSerial.println("AT+SAPBR=3,1,\"APN\",\"rcomnet\""); 
    delay(2000);
    toSerial();

 
    GprsSerial.println("AT+SAPBR=1,1");
    delay(3000);
    toSerial();
    Serial.write("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n");
}

/*Code for getting the outpout of CGGAT so as to know whether GPRS is activatd in the SIM or not */

boolean CGGAT(){
static byte dataBufferIndex = 0;

  while(GprsSerial.available()>0){
    char incomingByte = GprsSerial.read();
    if(incomingByte ==startChar1){

      dataBufferIndex = 0;
      storeString =true;
    }
    if(storeString){
      if(dataBufferIndex == DATABUFFERSIZE){
        dataBufferIndex = 0;
        break;
      }
      if(incomingByte=='\n'){
        CGGATdataBuffer[dataBufferIndex] = 0;
        return true;
      }
      else{
        CGGATdataBuffer[dataBufferIndex++]=incomingByte;
        CGGATdataBuffer[dataBufferIndex] = 0;
      }
    }
    else{
    }
  }
  return false;
}

