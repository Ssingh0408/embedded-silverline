#include <SoftwareSerial.h>
const int buttonPin = 2;    // the number of the pushbutton pin
const int ledPin = 13;      // the number of the LED pin
SoftwareSerial BluetoothSerial(10, 11); // RX | TX

// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin


long lastDebounceTime = 0;  
long debounceDelay = 20;    
int count = 0;
void setup() {
  
   Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BluetoothSerial.begin(9600); 
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);

  // set initial LED state
  digitalWrite(ledPin, ledState);
}

void loop() {
  // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);

  
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }
 
  if ((millis() - lastDebounceTime) > debounceDelay) {
   
    if (reading != buttonState) {
      buttonState = reading;

     
      if (buttonState == HIGH) {
        ledState = !ledState;
        count = count + 1;  //This is just for counting for how many times the switch is pressed or sword is touched.
    BluetoothSerial.print("$Player Sword touches oponent "); //$ added as to define the starting of the string
    BluetoothSerial.print(count);
    BluetoothSerial.print("times");
    BluetoothSerial.print("\n");
      }
      /*
      This code is only for testing purpose.This still need to be tested
      Adding feature to detect when there is not touch or switched pressed.
      */
      else {
             if(buttonState==LOW){
             BluetoothSerial.print("$OFF");
             BluetoothSerial.print("\n");
             }
             
      }
      
    }
  }
 

  digitalWrite(ledPin, ledState);

  
  lastButtonState = reading;
}
